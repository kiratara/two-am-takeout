This file is for taking notes as you go.

start: 2:16 PM

Starting:

Actions:

    > Forked

    > Cloned to local 

Run commands:

    > npm install

    > npm run serve

    > `go test ./server -v`

### Task 1: 
Message:

        main_test.go:55: 
            Error Trace:    main_test.go:55 
            `Error:          "{\"OK\":true}\n" does not contain "\"ok\":true"` 
            Test:           TestServerSuite/TestHealthCheck

I assumed this is what the 1st task is referrring to.
    Following the error message, I looked at the `main_test.go` file.
    The assertion at main_test.go:55 is expecting an "ok:true" \
    However, when you follow the endpoint that is being called - "/health" this 
    calls the ServeHealth method from the handler.go.
    The function `ServeHealth` returns an instance of the HealthBody struct.
    The HealthBody is defined with uppercased "OK", but the test is asserting 
    against an "ok" with lowercase. 
    The mismatch in spelling is causing the assertion to be false.
    The same can be seen when the value of `respString` is printed - it shows 
    {"OK":true} - validating the above theory.

### Task 2:
Likewise, after `npm run serve`, visited the website on `localhost:8080`, 
and then open the developer tool to see if there are any error or anything else of interest.

Can see `backend check failed, response code was 200, data was [object Object]` \
From the log - followed to file `Chat.vue` at line 118.
I can see an API call to `axios.get('http://localhost:3000/health')`
and similar to the test, this is also expecting an `res.data.ok === true`.
Updated the `res.data.ok` -> `res.data.OK`


### Task 3:
Based on the README.md, looked through the Vue files to see where the logic for user input is.

`@click="sendMessage(currentMessage)"` - I see that clicking on the SEND button
calls `sendMessageMethod` with `currentMessage` holding the text value.

`sendMessage` method then stringifies, with a unique id assigned to this entry.
Currently, there is only one input field - for `currentMessage`

I added a new input field for Name and moved both fields within a form. 
The form and the input fields are bound to currentSubmissions.currentMessage 
and currentSubmission.currentName attributes.

--- Stop time: 4:20

I was attempting to figure out a way to be able to access both of these
properties within the "sendMessage" method so that the stringified "message"
has both of these attributes. 
